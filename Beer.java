public class Beer {
	public static void main(String[] args) {
		int numOfBeer;
		String grammar = "bottles";
		
		for(numOfBeer = 9; numOfBeer > 0; numOfBeer--) {
			if(numOfBeer == 1) {
				grammar = "bottle";
			}
			
			System.out.println(numOfBeer + " " + grammar + " of beer on the wall, " +
			numOfBeer + " " + grammar + " of beer.");
			System.out.println("Take one down, pass it around,");
		}
		
		System.out.print("No more bottles of beer on the wall."); 	
	}
}
